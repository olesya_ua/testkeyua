CREATE TABLE category
(
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(100),
	parent_id INT,
	FOREIGN KEY (parent_id) REFERENCES category(id)
);

CREATE TABLE product
(
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(100),
	price  FLOAT,
	category_id INT,
	FOREIGN KEY (category_id) REFERENCES category(id)
);

INSERT INTO category (name, parent_id) VALUES ('Computers', null);
INSERT INTO category (name, parent_id) VALUES ('Laptops', 1);
INSERT INTO category (name, parent_id) VALUES ('Tablets', 1);
INSERT INTO category (name, parent_id) VALUES ('2 in 1 Laptops', 1);
INSERT INTO category (name, parent_id) VALUES ('Smartphones', null);

INSERT INTO product (name, price, category_id)
VALUES ('Apple MacBook Pro (13-inch, 2.3GHz Dual-Core Intel Core i5, 8GB RAM, 128GB SSD) - Space Gray', 1099.00, 2);
INSERT INTO product (name, price, category_id)
VALUES ('Lenovo Flex 14 2-in-1 Convertible Laptop, 14 Inch FHD, Touchscreen, AMD Ryzen 5 3500U Processor', 523.00, 2);
INSERT INTO product (name, price, category_id)
VALUES ('Asus C302CA-DHM4 Chromebook Flip 12.5-Inch Touchscreen Convertible Chromebook, Intel Core M3', 473.00, 4);
INSERT INTO product (name, price, category_id)
VALUES ('Samsung Galaxy Tab A SM-T580NZKAXAR 10.1-Inch 16 GB, Tablet (Black)', 277.99, 3);
INSERT INTO product (name, price, category_id)
VALUES ('Microsoft Surface Pro 6 (Intel Core i5, 8GB RAM, 128GB) - Black, Platinum', 797.49, 3);
INSERT INTO product (name, price, category_id)
VALUES ('Apple iPad Air 2, 64 GB, Space Gray', 208.93, 3);
INSERT INTO product (name, price, category_id)
VALUES ('Apple iPhone XS Max (64GB) - Silver', 1099.99, 5);

#1 - для заданного списка товаров получить названия всех категорий, в которых представлены товары
SELECT category.name
FROM category
JOIN product
	ON category.id = product.category_id
WHERE product.id IN (1,3,5,7);

#2 - для заданной категории получить список предложений всех товаров из этой категории. Каждая категория может иметь несколько подкатегорий
SELECT product.name, product.price, category.name
FROM product
JOIN category
	ON category.id = product.category_id
WHERE category.id = 1 OR category.parent_id = 1;

#3 - для заданного списка категорий получить количество уникальных товаров в каждой категории
SELECT category.name, count(*)
FROM category
JOIN product
	ON category.id = product.category_id
WHERE category.id IN (2,3,5)
GROUP BY category.id;

#4 - для заданного списка категорий получить количество единиц каждого товара, который входит в указанные категории
# не до конца поняла условие данного пункта