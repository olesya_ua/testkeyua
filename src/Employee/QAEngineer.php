<?php
namespace App\Employee;

use App\Employee\Action;

class QAEngineer extends AbstractEmployee implements
    Action\CanTestCodeInterface,
    Action\CanCommunicateWithManagerInterface,
    Action\CanCreateTaskInterface
{

}
