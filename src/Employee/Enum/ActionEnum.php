<?php
namespace App\Employee\Enum;

class ActionEnum
{
    public const WRITE_CODE = 'writeCode';
    public const TEST_CODE = 'testCode';
    public const DRAW = 'draw';
    public const COMMUNICATE_WITH_MANAGER = 'communicateWithManager';
    public const CREATE_TASK = 'createTask';
}
