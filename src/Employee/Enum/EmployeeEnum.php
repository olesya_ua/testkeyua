<?php
namespace App\Employee\Enum;

class EmployeeEnum
{
    public const PROGRAMMER = 'programmer';
    public const QA_ENGINEER = 'qaEngineer';
    public const DESIGNER = 'designer';
    public const MANAGER = 'manager';
}
