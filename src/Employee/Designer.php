<?php
namespace App\Employee;

use App\Employee\Action;

class Designer extends AbstractEmployee implements
    Action\CanDrawInterface,
    Action\CanCommunicateWithManagerInterface
{

}
