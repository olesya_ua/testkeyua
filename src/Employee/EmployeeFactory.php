<?php
namespace App\Employee;

use App\Employee\Enum\EmployeeEnum;

class EmployeeFactory
{
    /**
     * @param string $position
     *
     * @return AbstractEmployee
     *
     * @throws \DomainException
     */
    public function create(string $position): AbstractEmployee
    {
        switch ($position) {
            case EmployeeEnum::PROGRAMMER:
                return $this->createProgrammer();
            case EmployeeEnum::QA_ENGINEER:
                return $this->createQAEngineer();
            case EmployeeEnum::DESIGNER:
                return $this->createDesigner();
            case EmployeeEnum::MANAGER:
                return $this->createManager();
            default:
                throw new \DomainException(sprintf('Unknown position %s', $position));
        }
    }

    public function createProgrammer(): Programmer
    {
        return new Programmer();
    }

    public function createQAEngineer(): QAEngineer
    {
        return new QAEngineer();
    }

    public function createDesigner(): Designer
    {
        return new Designer();
    }

    public function createManager(): Manager
    {
        return new Manager();
    }
}
