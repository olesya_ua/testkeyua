<?php
namespace App\Employee;

use App\Employee\Action;

class Programmer extends AbstractEmployee implements
    Action\CanWriteCodeInterface,
    Action\CanTestCodeInterface,
    Action\CanCommunicateWithManagerInterface
{

}
