<?php
namespace App\Employee;

use App\Employee\Action\CanCommunicateWithManagerInterface;
use App\Employee\Action\CanCreateTaskInterface;
use App\Employee\Action\CanDrawInterface;
use App\Employee\Action\CanTestCodeInterface;
use App\Employee\Action\CanWriteCodeInterface;
use App\Employee\Enum\ActionEnum;

class AbstractEmployee
{
    public function canWriteCode(): bool
    {
        return $this instanceof CanWriteCodeInterface;
    }

    public function canTestCode(): bool
    {
        return $this instanceof CanTestCodeInterface;
    }

    public function canDraw(): bool
    {
        return $this instanceof CanDrawInterface;
    }

    public function canCommunicateWithManager(): bool
    {
        return $this instanceof CanCommunicateWithManagerInterface;
    }

    public function canCreateTask(): bool
    {
        return $this instanceof CanCreateTaskInterface;
    }

    public function canDoAction($action): bool
    {
        switch ($action) {
            case ActionEnum::WRITE_CODE:
                return $this->canWriteCode();
            case ActionEnum::TEST_CODE:
                return $this->canTestCode();
            case ActionEnum::DRAW:
                return $this->canDraw();
            case ActionEnum::COMMUNICATE_WITH_MANAGER:
                return $this->canCommunicateWithManager();
            case ActionEnum::CREATE_TASK:
                return $this->canCreateTask();
            default:
                throw new \DomainException(sprintf('Unknown action %s', $action));
        }
    }

    public function getList(): array
    {
        $actionList = [];

        if ($this instanceof CanWriteCodeInterface) {
            $actionList[] = ActionEnum::WRITE_CODE;
        }

        if ($this instanceof CanTestCodeInterface) {
            $actionList[] = ActionEnum::TEST_CODE;
        }

        if ($this instanceof CanDrawInterface) {
            $actionList[] = ActionEnum::DRAW;
        }

        if ($this instanceof CanCreateTaskInterface) {
            $actionList[] = ActionEnum::CREATE_TASK;
        }

        if ($this instanceof CanCommunicateWithManagerInterface) {
            $actionList[] = ActionEnum::COMMUNICATE_WITH_MANAGER;
        }

        return $actionList;
    }
}
