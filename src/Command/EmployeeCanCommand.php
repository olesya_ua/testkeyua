<?php
namespace App\Command;

use App\Employee\EmployeeFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmployeeCanCommand extends Command
{
    protected static $defaultName = 'employee:can';

    /** @var EmployeeFactory */
    private $employeeFactory;

    public function __construct(EmployeeFactory $employeeFactory, $name = null)
    {
        parent::__construct($name);

        $this->employeeFactory = $employeeFactory;
    }

    protected function configure()
    {
        $this->setHelp('This command allows you to verify that an employee can do a specific action.');
        $this->addArgument('position', InputArgument::REQUIRED, 'The position of employee.');
        $this->addArgument('action', InputArgument::REQUIRED, 'The action of employee.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employeePosition = $input->getArgument('position');
        $employeeAction = $input->getArgument('action');

        $employee = $this->employeeFactory->create($employeePosition);

        $output->writeln($employee->canDoAction($employeeAction) ? 'true' : 'false');
    }
}