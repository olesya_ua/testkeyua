<?php
namespace App\Command;

use App\Employee\EmployeeFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompanyEmployeeCommand extends Command
{
    protected static $defaultName = 'company:employee';

    /** @var EmployeeFactory */
    private $employeeFactory;

    public function __construct(EmployeeFactory $employeeFactory, $name = null)
    {
        parent::__construct($name);

        $this->employeeFactory = $employeeFactory;
    }

    protected function configure()
    {
        $this->setHelp('This command allows you to show what an employee can do.');
        $this->addArgument('position', InputArgument::REQUIRED, 'The position of employee.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employeePosition = $input->getArgument('position');

        $employee = $this->employeeFactory->create($employeePosition);

        $output->writeln($employee->getList());
    }
}